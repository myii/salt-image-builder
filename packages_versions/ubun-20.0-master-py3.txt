ubun-20.0-master-py3
Salt install method: git
# salt-call --versions-report
Salt Version:
          Salt: 3006.0+0na.d937173
 
Dependency Versions:
          cffi: Not Installed
      cherrypy: Not Installed
      dateutil: Not Installed
     docker-py: Not Installed
         gitdb: Not Installed
     gitpython: Not Installed
        Jinja2: 3.1.2
       libgit2: Not Installed
      M2Crypto: Not Installed
          Mako: Not Installed
       msgpack: 1.0.4
  msgpack-pure: Not Installed
  mysql-python: Not Installed
     pycparser: Not Installed
      pycrypto: Not Installed
  pycryptodome: 3.16.0
        pygit2: Not Installed
        Python: 3.8.10 (default, Nov 14 2022, 12:59:47)
  python-gnupg: Not Installed
        PyYAML: 6.0
         PyZMQ: 24.0.1
         smmap: Not Installed
       timelib: Not Installed
       Tornado: 4.5.3
           ZMQ: 4.3.4
 
System Versions:
          dist: ubuntu 20.04 Focal Fossa
        locale: utf-8
       machine: x86_64
       release: 5.4.109+
        system: Linux
       version: Ubuntu 20.04 Focal Fossa
 
# salt-call --local grains.item init kernel kernelrelease kernelversion locale_info lsb_distrib_codename lsb_distrib_id lsb_distrib_release os os_family osarch osbuild oscodename osfinger osfullname osmajorrelease osmanufacturer osrelease osrelease_info osversion pythonexecutable pythonpath pythonversion saltpath saltversion saltversioninfo systemd virtual virtual_subtype zmqversion
local:
    ----------
    init:
        unknown
    kernel:
        Linux
    kernelrelease:
        5.4.109+
    kernelversion:
        #1 SMP Wed Jun 16 20:00:10 PDT 2021
    locale_info:
        ----------
        defaultencoding:
            UTF-8
        defaultlanguage:
            en_US
        detectedencoding:
            utf-8
        timezone:
            unknown
    lsb_distrib_codename:
        focal
    lsb_distrib_id:
        Ubuntu
    lsb_distrib_release:
        20.04
    os:
        Ubuntu
    os_family:
        Debian
    osarch:
        amd64
    osbuild:
    oscodename:
        focal
    osfinger:
        Ubuntu-20.04
    osfullname:
        Ubuntu
    osmajorrelease:
        20
    osmanufacturer:
    osrelease:
        20.04
    osrelease_info:
        - 20
        - 4
    osversion:
    pythonexecutable:
        /usr/bin/python3
    pythonpath:
        - /usr/local/bin
        - /usr/lib/python38.zip
        - /usr/lib/python3.8
        - /usr/lib/python3.8/lib-dynload
        - /usr/local/lib/python3.8/dist-packages
        - /usr/lib/python3/dist-packages
    pythonversion:
        - 3
        - 8
        - 10
        - final
        - 0
    saltpath:
        /usr/local/lib/python3.8/dist-packages/salt
    saltversion:
        3006.0+0na.d937173
    saltversioninfo:
        - 3006
        - 0
    systemd:
        ----------
        features:
            +PAM +AUDIT +SELINUX +IMA +APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=hybrid
        version:
            245
    virtual:
        container
    virtual_subtype:
        Docker
    zmqversion:
        4.3.4
# salt-call --local pkg.list_pkgs
local:
    ----------
    adduser:
        3.118ubuntu2
    apt:
        2.0.9
    apt-transport-https:
        2.0.9
    base-files:
        11ubuntu5.6
    base-passwd:
        3.5.47
    bash:
        5.0-6ubuntu1.2
    binutils:
        2.34-6ubuntu1.4
    binutils-common:
        2.34-6ubuntu1.4
    binutils-x86-64-linux-gnu:
        2.34-6ubuntu1.4
    bsdutils:
        1:2.34-0.1ubuntu9.3
    bzip2:
        1.0.8-2
    ca-certificates:
        20211016ubuntu0.20.04.1
    coreutils:
        8.30-3ubuntu2
    cpp:
        4:9.3.0-1ubuntu2
    cpp-9:
        9.4.0-1ubuntu1~20.04.1
    curl:
        7.68.0-1ubuntu2.14
    dash:
        0.5.10.2-6
    debconf:
        1.5.73
    debianutils:
        4.9.1
    diffutils:
        1:3.7-3
    dirmngr:
        2.2.19-3ubuntu2.2
    distro-info-data:
        0.43ubuntu1.11
    dpkg:
        1.19.7ubuntu3.2
    e2fsprogs:
        1.45.5-2ubuntu1.1
    fdisk:
        2.34-0.1ubuntu9.3
    findutils:
        4.7.0-1ubuntu1
    gcc:
        4:9.3.0-1ubuntu2
    gcc-10-base:
        10.3.0-1ubuntu1~20.04
    gcc-9:
        9.4.0-1ubuntu1~20.04.1
    gcc-9-base:
        9.4.0-1ubuntu1~20.04.1
    git:
        1:2.25.1-1ubuntu3.6
    git-man:
        1:2.25.1-1ubuntu3.6
    gnupg:
        2.2.19-3ubuntu2.2
    gnupg-l10n:
        2.2.19-3ubuntu2.2
    gnupg-utils:
        2.2.19-3ubuntu2.2
    gpg:
        2.2.19-3ubuntu2.2
    gpg-agent:
        2.2.19-3ubuntu2.2
    gpg-wks-client:
        2.2.19-3ubuntu2.2
    gpg-wks-server:
        2.2.19-3ubuntu2.2
    gpgconf:
        2.2.19-3ubuntu2.2
    gpgsm:
        2.2.19-3ubuntu2.2
    gpgv:
        2.2.19-3ubuntu2.2
    grep:
        3.4-1
    gzip:
        1.10-0ubuntu4.1
    hostname:
        3.23
    init-system-helpers:
        1.57
    libacl1:
        2.2.53-6
    libapparmor1:
        2.13.3-7ubuntu5.1
    libapt-pkg6.0:
        2.0.9
    libargon2-1:
        0~20171227-0.2
    libasan5:
        9.4.0-1ubuntu1~20.04.1
    libasn1-8-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libassuan0:
        2.5.3-7ubuntu2
    libatomic1:
        10.3.0-1ubuntu1~20.04
    libattr1:
        1:2.4.48-5
    libaudit-common:
        1:2.8.5-2ubuntu6
    libaudit1:
        1:2.8.5-2ubuntu6
    libbinutils:
        2.34-6ubuntu1.4
    libblkid1:
        2.34-0.1ubuntu9.3
    libbrotli1:
        1.0.7-6ubuntu0.1
    libbsd0:
        0.10.0-1
    libbz2-1.0:
        1.0.8-2
    libc-bin:
        2.31-0ubuntu9.9
    libc-dev-bin:
        2.31-0ubuntu9.9
    libc6:
        2.31-0ubuntu9.9
    libc6-dev:
        2.31-0ubuntu9.9
    libcap-ng0:
        0.7.9-2.1build1
    libcap2:
        1:2.32-1
    libcbor0.6:
        0.6.0-0ubuntu1
    libcc1-0:
        10.3.0-1ubuntu1~20.04
    libcom-err2:
        1.45.5-2ubuntu1.1
    libcrypt-dev:
        1:4.4.10-10ubuntu4
    libcrypt1:
        1:4.4.10-10ubuntu4
    libcryptsetup12:
        2:2.2.2-3ubuntu2.4
    libctf-nobfd0:
        2.34-6ubuntu1.4
    libctf0:
        2.34-6ubuntu1.4
    libcurl3-gnutls:
        7.68.0-1ubuntu2.14
    libcurl4:
        7.68.0-1ubuntu2.14
    libdb5.3:
        5.3.28+dfsg1-0.6ubuntu2
    libdebconfclient0:
        0.251ubuntu1
    libdevmapper1.02.1:
        2:1.02.167-1ubuntu1
    libedit2:
        3.1-20191231-1
    liberror-perl:
        0.17029-1
    libexpat1:
        2.2.9-1ubuntu0.6
    libexpat1-dev:
        2.2.9-1ubuntu0.6
    libext2fs2:
        1.45.5-2ubuntu1.1
    libfdisk1:
        2.34-0.1ubuntu9.3
    libffi7:
        3.3-4
    libfido2-1:
        1.3.1-1ubuntu2
    libgcc-9-dev:
        9.4.0-1ubuntu1~20.04.1
    libgcc-s1:
        10.3.0-1ubuntu1~20.04
    libgcrypt20:
        1.8.5-5ubuntu1.1
    libgdbm-compat4:
        1.18.1-5
    libgdbm6:
        1.18.1-5
    libgmp10:
        2:6.2.0+dfsg-4ubuntu0.1
    libgnutls30:
        3.6.13-2ubuntu1.7
    libgomp1:
        10.3.0-1ubuntu1~20.04
    libgpg-error0:
        1.37-1
    libgssapi-krb5-2:
        1.17-6ubuntu4.1
    libgssapi3-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libhcrypto4-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libheimbase1-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libheimntlm0-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libhogweed5:
        3.5.1+really3.5.1-2ubuntu0.2
    libhx509-5-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libidn2-0:
        2.2.0-2
    libip4tc2:
        1.8.4-3ubuntu2
    libisl22:
        0.22.1-1
    libitm1:
        10.3.0-1ubuntu1~20.04
    libjson-c4:
        0.13.1+dfsg-7ubuntu0.3
    libk5crypto3:
        1.17-6ubuntu4.1
    libkeyutils1:
        1.6-6ubuntu1.1
    libkmod2:
        27-1ubuntu2.1
    libkrb5-26-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libkrb5-3:
        1.17-6ubuntu4.1
    libkrb5support0:
        1.17-6ubuntu4.1
    libksba8:
        1.3.5-2ubuntu0.20.04.1
    libldap-2.4-2:
        2.4.49+dfsg-2ubuntu1.9
    libldap-common:
        2.4.49+dfsg-2ubuntu1.9
    liblsan0:
        10.3.0-1ubuntu1~20.04
    liblz4-1:
        1.9.2-2ubuntu0.20.04.1
    liblzma5:
        5.2.4-1ubuntu1.1
    libmount1:
        2.34-0.1ubuntu9.3
    libmpc3:
        1.1.0-1
    libmpdec2:
        2.4.2-3
    libmpfr6:
        4.0.2-1
    libncurses6:
        6.2-0ubuntu2
    libncursesw6:
        6.2-0ubuntu2
    libnettle7:
        3.5.1+really3.5.1-2ubuntu0.2
    libnghttp2-14:
        1.40.0-1build1
    libnpth0:
        1.6-1
    libp11-kit0:
        0.23.20-1ubuntu0.1
    libpam-modules:
        1.3.1-5ubuntu4.3
    libpam-modules-bin:
        1.3.1-5ubuntu4.3
    libpam-runtime:
        1.3.1-5ubuntu4.3
    libpam0g:
        1.3.1-5ubuntu4.3
    libpcre2-8-0:
        10.34-7ubuntu0.1
    libpcre3:
        2:8.39-12ubuntu0.1
    libperl5.30:
        5.30.0-9ubuntu0.3
    libprocps8:
        2:3.3.16-1ubuntu2.3
    libpsl5:
        0.21.0-1ubuntu1
    libpython3-dev:
        3.8.2-0ubuntu2
    libpython3-stdlib:
        3.8.2-0ubuntu2
    libpython3.8:
        3.8.10-0ubuntu1~20.04.6
    libpython3.8-dev:
        3.8.10-0ubuntu1~20.04.6
    libpython3.8-minimal:
        3.8.10-0ubuntu1~20.04.6
    libpython3.8-stdlib:
        3.8.10-0ubuntu1~20.04.6
    libquadmath0:
        10.3.0-1ubuntu1~20.04
    libreadline8:
        8.0-4
    libroken18-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    librtmp1:
        2.4+20151223.gitfa8646d.1-2build1
    libsasl2-2:
        2.1.27+dfsg-2ubuntu0.1
    libsasl2-modules-db:
        2.1.27+dfsg-2ubuntu0.1
    libseccomp2:
        2.5.1-1ubuntu1~20.04.2
    libselinux1:
        3.0-1build2
    libsemanage-common:
        3.0-1build2
    libsemanage1:
        3.0-1build2
    libsepol1:
        3.0-1ubuntu0.1
    libsmartcols1:
        2.34-0.1ubuntu9.3
    libsqlite3-0:
        3.31.1-4ubuntu0.5
    libss2:
        1.45.5-2ubuntu1.1
    libssh-4:
        0.9.3-2ubuntu2.2
    libssl1.1:
        1.1.1f-1ubuntu2.16
    libstdc++6:
        10.3.0-1ubuntu1~20.04
    libsystemd0:
        245.4-4ubuntu3.19
    libtasn1-6:
        4.16.0-2
    libtinfo6:
        6.2-0ubuntu2
    libtsan0:
        10.3.0-1ubuntu1~20.04
    libubsan1:
        10.3.0-1ubuntu1~20.04
    libudev1:
        245.4-4ubuntu3.19
    libunistring2:
        0.9.10-2
    libuuid1:
        2.34-0.1ubuntu9.3
    libwind0-heimdal:
        7.7.0+dfsg-1ubuntu1.2
    libwrap0:
        7.6.q-30
    libzstd1:
        1.4.4+dfsg-3ubuntu0.1
    linux-libc-dev:
        5.4.0-135.152
    locales:
        2.31-0ubuntu9.9
    login:
        1:4.8.1-1ubuntu5.20.04.4
    logsave:
        1.45.5-2ubuntu1.1
    lsb-base:
        11.1.0ubuntu2
    lsb-release:
        11.1.0ubuntu2
    mawk:
        1.3.4.20200120-2
    mime-support:
        3.64ubuntu1
    mount:
        2.34-0.1ubuntu9.3
    ncurses-base:
        6.2-0ubuntu2
    ncurses-bin:
        6.2-0ubuntu2
    net-tools:
        1.60+git20180626.aebd88e-1ubuntu1
    openssh-client:
        1:8.2p1-4ubuntu0.5
    openssh-server:
        1:8.2p1-4ubuntu0.5
    openssh-sftp-server:
        1:8.2p1-4ubuntu0.5
    openssl:
        1.1.1f-1ubuntu2.16
    passwd:
        1:4.8.1-1ubuntu5.20.04.4
    perl:
        5.30.0-9ubuntu0.3
    perl-base:
        5.30.0-9ubuntu0.3
    perl-modules-5.30:
        5.30.0-9ubuntu0.3
    pinentry-curses:
        1.1.0-3build1
    procps:
        2:3.3.16-1ubuntu2.3
    python-apt-common:
        2.0.0ubuntu0.20.04.8
    python-pip-whl:
        20.0.2-5ubuntu1.6
    python3:
        3.8.2-0ubuntu2
    python3-apt:
        2.0.0ubuntu0.20.04.8
    python3-dev:
        3.8.2-0ubuntu2
    python3-distutils:
        3.8.10-0ubuntu1~20.04
    python3-lib2to3:
        3.8.10-0ubuntu1~20.04
    python3-minimal:
        3.8.2-0ubuntu2
    python3-pip:
        20.0.2-5ubuntu1.6
    python3-pkg-resources:
        45.2.0-1
    python3-setuptools:
        45.2.0-1
    python3-wheel:
        0.34.2-1
    python3.8:
        3.8.10-0ubuntu1~20.04.6
    python3.8-dev:
        3.8.10-0ubuntu1~20.04.6
    python3.8-minimal:
        3.8.10-0ubuntu1~20.04.6
    readline-common:
        8.0-4
    sed:
        4.7-1
    sensible-utils:
        0.0.12+nmu1
    sudo:
        1.8.31-1ubuntu1.2
    systemd:
        245.4-4ubuntu3.19
    systemd-sysv:
        245.4-4ubuntu3.19
    systemd-timesyncd:
        245.4-4ubuntu3.19
    sysvinit-utils:
        2.96-2.1ubuntu1
    tar:
        1.30+dfsg-7ubuntu0.20.04.2
    ubuntu-keyring:
        2020.02.11.4
    ucf:
        3.0038+nmu1
    udev:
        245.4-4ubuntu3.19
    util-linux:
        2.34-0.1ubuntu9.3
    wget:
        1.20.3-1ubuntu2
    zlib1g:
        1:1.2.11.dfsg-2ubuntu1.5
    zlib1g-dev:
        1:1.2.11.dfsg-2ubuntu1.5
# salt-call --local pip.list
local:
    ----------
    Jinja2:
        3.1.2
    MarkupSafe:
        2.1.1
    PyYAML:
        6.0
    certifi:
        2022.12.7
    charset-normalizer:
        2.1.1
    contextvars:
        2.4
    distro:
        1.8.0
    idna:
        3.4
    immutables:
        0.19
    jmespath:
        1.0.1
    msgpack:
        1.0.4
    pip:
        20.0.2
    psutil:
        5.9.4
    pycryptodomex:
        3.16.0
    python-apt:
        2.0.0+ubuntu0.20.4.8
    pyzmq:
        24.0.1
    requests:
        2.28.1
    salt:
        3006.0+0na.d937173
    setuptools:
        45.2.0
    urllib3:
        1.26.13
    wheel:
        0.34.2
# salt-call --local locale.list_avail
local:
    - C
    - C.UTF-8
    - POSIX
    - en_US.utf8
